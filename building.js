function Building(speed) {
  this.spacing = 175;
  // this.top = random(height / 6, 3 / 4 * height);
  // this.bottom = height - (this.top + this.spacing);
  this.bottom = random(height / 6, 2.5 / 5 * height);
  this.top = 110;
  this.x = width;
  this.w = 80;
  this.speed = speed;

  this.highlightHit = false;
  this.highlightDetection = false;
  
  this.detected = function(airplane){
     if(airplane.y < this.top){
       this.highlightDetection = true;
       return true;
     }else{
       this.highlightDetection = false;
       return false;
     }
  }

  this.hits = function(airplane) {
    if (airplane.y > height - this.bottom && 
          airplane.x > this.x && 
          airplane.x < this.x + this.w
       ) {
        this.highlightHit = true;
        return true;
    }
    this.highlightHit = false;
    return false;
  }

  this.show = function() {
    
    if (this.highlightDetection) {
      fill(2,21,2,150);
    }else{
      fill(59,98,48,50);
    }
  
    rect(0, 0, 640, this.top);
   
    if (this.highlightHit) {
      fill(150, 0, 0);
    }else{
      fill(92,77,69,200);
    }
    
    rect(this.x, height - this.bottom, this.w, this.bottom);
    // circle(this.x, height - this.bottom, this.w, this.bottom); 
  }

  this.update = function() {
    this.x -= this.speed;
  }

  this.offscreen = function() {
    if (this.x < -this.w) {
      return true;
    } else {
      return false;
    }
  }


}