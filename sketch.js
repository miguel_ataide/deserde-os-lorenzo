const TARGET_SCORE = 50000;
const SOUND_VOLUME = 0.05;

let airplane;
let buildings = [];
let bg;
let gameOverBG;
let familyImage;
let logoImage;
let score = 0;
let speed = 6;
let detectionPercentage = 0;
let damagePercentage = 0;
let gameOver = false;
let youWin = false;
let bgSound;
let crashSound;
let crashSoundDone = false;
let radarSound;
let impactSound;
let victorySound;
let startSound;
let victoryCutScene;
let playingVictory = false;
let victoryPlayed = false;
let restart = false;
let restartButton;
let startButton;
let started = false;
let storyScreen = false;

let content = 'WELCOME TO THE INFINITE SCROLL'; 
let yStart = 0;
let customFont; 
let storyArray = 
    ['Você é Orestes Lorenzo, um piloto de caça da força aérea', 
     'cubana que está tentando fugir, com sua família, ', 
     'do regime ditatorial cubano no início da década de 90. ', 
     'Em 1991, cerca de um ano antes desta missão,', 
     'Orestes deserdou do regime cubano',
     'quando furtou um um jato militar MiG-23', 
     'e conseguiu abrigo nos EUA.', 
     'Ele foi considerado pelo regime como um traidor, ', 
     'assim como sua família que ficou em Cuba.', 
     'Orestes Lorenzo tentou de todas as formas diplomáticas', 
     'e políticas possíveis imigrar sua família para os EUA.', 
     'Sem sucesso, ele decidiu conseguir', 
     'um avião para voltar à Cuba', 
     'em uma missão para resgatar sua esposa e filhos. ', 
     'Assim, seu objetivo é utilizar um avião monomotor, ', 
     'avariado, para conseguir se afastar dos limites cubanos', 
     'a fim de conseguir abrigo político em território', 
     'americano para a família Lorenzo.'];

function preload(){
  
  customFont = loadFont('fonts/SpaceMono-Regular.ttf'); 
  
  bg = loadImage('assets/city-bg.jpg');
  gameOverBG = loadImage('assets/crash.jpg');
  familyImage = loadImage('assets/family.jpg');
  logoImage = loadImage('assets/logo.jpg');
  
  bgSound = loadSound('sounds/escape.mp3');
  victorySound = loadSound('sounds/victory.mp3');
  crashSound = loadSound('sounds/crash.mp3');
  radarSound = loadSound('sounds/radar.mp3');
  impactSound = loadSound('sounds/impact.mp3');
  startSound = loadSound('sounds/start.mp3');
  
  bgSound.setVolume(SOUND_VOLUME/2);
  victorySound.setVolume(SOUND_VOLUME);
  crashSound.setVolume(SOUND_VOLUME);
  radarSound.setVolume(SOUND_VOLUME);
  impactSound.setVolume(SOUND_VOLUME);
  startSound.setVolume(SOUND_VOLUME);
  
  for(let i = 0; i < 5; i++){
    storyArray.unshift("\n");
  }
  
  storyArray.unshift("PRESS SPACEBAR TO CONTINUE");
  
  for(let i = 0; i < 5; i++){
    storyArray.unshift("\n");
  }
}

function setup() {
  createCanvas(640, 480);
  
  if(started){
    airplane = new Airplane();
    buildings.push(new Building(speed));
  }
  
}

function draw() {
    
      if(!started){

          if(storyScreen){

            textFont(customFont);
            textAlign(CENTER, CENTER);
            textSize(17); 

            background(0);
            frameRate(30)

            let index = 0;

            for (let y = yStart; y < height; y += 28) { 

              if(index == storyArray.length){
                index = 0;
              }

              fill(59, y / 2 + 55, 48); 
              content = storyArray[index];
              text(content, width / 2, y);
              index++;
            }

            yStart--;  

            return;
      }
 
    
      background(logoImage);
    
      fill(0, 0, 0);
      textSize(30);

      const title = "DESERDE OS LORENZO";
      textStyle(BOLDITALIC);
      text(title, 145, 200)

      if(!startSound.isPlaying()){
        startSound.play();
      }

      startButton = createButton('Start');
      startButton.position(267,210);
      startButton.style("font-family", "Comic Sans MS");
      startButton.style("font-size", "48px");
      startButton.style("border-radius", "60%");
      startButton.style("background-color", "#555555");
      startButton.style("color", "white");
      startButton.style("border", "2px solid");
    
      startButton.mousePressed(function(){
        storyScreen = true;
        reset();
      });
   
    return;
  }
  
  if(!gameOver){
    
    if(youWin){
    
      score = TARGET_SCORE;
      bgSound.stop();

      if(!playingVictory){
  
        createCanvas(0, 0);
        playingVictory = true;
        victoryCutScene = createVideo('videos/arrival.mp4');
        victoryCutScene.play();
      }
      
      if(!victoryPlayed && victoryCutScene.duration() - victoryCutScene.time() == 0){
    
        victoryPlayed = true;
        victoryCutScene.hide();

        createCanvas(640, 480);
        background(familyImage);
        
        restartButton = createButton('Restart');
        restartButton.position(20, 90);
        restartButton.style("font-family", "Comic Sans MS");
        restartButton.style("font-size", "48px");
        restartButton.style("border-radius", "60%");
        restartButton.style("background-color", "#555555");
        restartButton.style("color", "white");
        restartButton.style("border", "2px solid");
        
        restartButton.mousePressed(function(){
          restart = true;
        });
     
      }
      
      if(!victorySound.isPlaying()){
        victorySound.play();
      }
    
      fill(0, 0, 0);
      textSize(35);

      const gameOverText = "YOU WIN!";
      text(gameOverText, 20, 40)

      const scoreText = "SCORE: " + score;
      text(scoreText, 20, 70)
      
      if(restart){
        victorySound.stop();
        this.reset();
      }
      
    }else{
      
      if(score > TARGET_SCORE)
        youWin = true;

      if(!bgSound.isPlaying())
        bgSound.play();

      background(bg);

      for (let i = buildings.length-1; i >= 0; i--) {
        buildings[i].show();
        buildings[i].update();
        drawScore();

        if (buildings[i].hits(airplane)) {

          if(!impactSound.isPlaying()){
            impactSound.play();
          }

          damagePercentage = 100;
        }

        if (buildings[i].detected(airplane)) {

            if(!radarSound.isPlaying()) {
              radarSound.play();
            } 

          detectionPercentage += getRandomArbitrary(0.05, 0.25);
        }else if(detectionPercentage > 0){
          detectionPercentage -= 0.01;
        }

        if(detectionPercentage < 0.0){
          detectionPercentage = 0;
        }

        if(damagePercentage >= 100){
          damagePercentage = 100;
          gameOver = true;
        }

        if(detectionPercentage >= 100){
          detectionPercentage = 100;
          gameOver = true;
        }

        if (buildings[i].offscreen()) {
          buildings.splice(i, 1);
        }
      }

      airplane.update();
      airplane.show();

      if (frameCount % 75 == 0) {
        speed += 0.1;

        if(speed > 0){
          speed = 6;
        }

        buildings.push(new Building(speed));
      }
    }
    
  } else{

    bgSound.stop();
    
    background(gameOverBG);
    
    fill(255, 255, 255);
    textSize(35);

    const gameOverText = "GAME OVER";
    text(gameOverText, 320, 240)

    const scoreText = "SCORE: " + score;
    text(scoreText, 320, 270)
    
    if(crashSound.isPlaying() == false && !crashSoundDone) {
      crashSound.play();
      crashSoundDone = true;
    } 
    
     restartButton = createButton('Try Again');
        restartButton.position(20, 90);
        restartButton.style("font-family", "Comic Sans MS");
        restartButton.style("font-size", "42px");
        restartButton.style("border-radius", "70%");
        restartButton.style("background-color", "#555555");
        restartButton.style("color", "white");
        restartButton.style("border", "2px solid");
        
        restartButton.mousePressed(function(){
          restart = true;
        });
    
    if(restart){
      reset();
    }
    
  }
  
}

function reset(){
    
  // airplane;
  buildings = [];
  // bg;
  // gameOverBG;
  // familyImage;
  score = 0;
  speed = 6;
  detectionPercentage = 0;
  damagePercentage = 0;
  gameOver = false;
  youWin = false;
  // bgSound;
  // crashSound;
  crashSoundDone = false;
  // radarSound;
  // impactSound;
  // victorySound;
  // victoryCutScene;
  playingVictory = false;
  victoryPlayed = false;
  restart = false;
  // started = true;
  //restartButton.remove();
   
  frameRate(60);
  removeElements();
  setup();
}

function keyPressed() {
  if (key == ' ') {
    
    if(storyScreen){
      started = true;
      storyScreen = false;
      startSound.stop();
      reset();
    }else{
      airplane.up();
    }
    
  }
}

function drawScore(){
  
  score += 1*speed;
  fill(255, 255, 255);
  textSize(24);            
  textAlign(LEFT, LEFT);
  const scoreText = "SCORE: " + score;
  text(scoreText, 20, 470)
  
  const detectionText = "DETECTION: " + Math.round((detectionPercentage + Number.EPSILON) * 100) / 100 + "%";
  text(detectionText, 20, 440)
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
