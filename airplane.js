let img;

function Airplane() {
  
  img = loadImage('assets/airplane.png');
  
  this.y = height/2;
  this.x = 64;

  this.gravity = 0.2;
  this.lift = -12;
  this.velocity = 0;
  
  this.show = function() {
    
    image(img, this.x, this.y);
    noFill();
    
//     rect(this.x,this.y,100,40);

    // fill(255);
    // ellipse(this.x, this.y, 32, 32);
  }

  this.up = function() {
    this.velocity += this.lift;
  }

  this.update = function() {
    this.velocity += this.gravity;
    
    this.y += this.velocity;

    if (this.y > height) {
      this.y = height;
      this.velocity = 0;
    }

    if (this.y < 0) {
      this.y = 0;
      this.velocity = 0;
    }
    
    if(this.y == 480){
      this.velocity += 0.8*this.lift;
    }
    
    // console.log(this.gravity);
    // console.log(this.velocity)
    // console.log(this.lift);
    // console.log(this.x);
    // console.log(this.y);

  }

}
