# Deserde os Lorenzo

## Como jogar

O projeto é executável em ambientes Javascript. Nos seguintes links é possível jogar pelo navegador:

[P5 Editor](https://editor.p5js.org/miguel.oliveira.ataide/sketches/Ra3PuOPUn)

[P5 Editor - Fullscreen](https://editor.p5js.org/miguel.oliveira.ataide/full/Ra3PuOPUn)

## Documentação

[Game Design Document (GDD)](https://docs.google.com/document/d/1MS8FXy8O_gCkoK-vz5NAzAQGJriJs0-IAmdmgabDtKs/edit?usp=sharing)

## Dependências

- p5.js
